<!DOCTYPE html>
<html lang="en">
<head>
<title>Health Insurance Premium</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">

		<div id="signupbox" style="margin-top: 50px"
			class="mainbox col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title">Health Insurance Premium</div>
					<div
						style="float: right; font-size: 85%; position: relative; top: -10px">
						<a id="signinlink" href="/accounts/login/"></a>
					</div>
				</div>
				<div class="panel-body">
					<form method="post" action="health.jsp">
						<input type='hidden' name='csrfmiddlewaretoken'
							value='XFe2rTYl9WOpV8U6X5CfbIuOZOELJ97S' />


						<form class="form-horizontal" method="post" >
							<div id="div_id_name" class="form-group required">
								<label for="id_name"
									class="control-label col-md-4  requiredField"> Full
									Name<span class="asteriskField">*</span>
								</label>
								<div class="controls col-md-8 ">
									<input class="input-md textinput textInput form-control"
										id="id_name" name="name" required
										placeholder="Your Frist name and Last name"
										style="margin-bottom: 10px" type="text" />
								</div>
							</div>
							<div id="div_id_gender" class="form-group required">
								<label for="id_gender"
									class="control-label col-md-4  requiredField"> Gender<span
									class="asteriskField">*</span>
								</label>
								<div class="controls col-md-8 " style="margin-bottom: 10px">
									<label class="radio-inline"> <input type="radio"
										name="gender" id="id_gender_1" value="M"
										style="margin-bottom: 10px">Male
									</label> <label class="radio-inline"> <input type="radio"
										name="gender" id="id_gender_2" value="F"
										style="margin-bottom: 10px">Female
									</label>
								</div>
							</div>
							<div id="div_id_company" class="form-group required">
								<label for="id_age"
									class="control-label col-md-4  requiredField"> AGE<span
									class="asteriskField">*</span>
								</label>
								<div class="controls col-md-8 ">
									<input class="input-md textinput textInput form-control"
										id="id_age" name="age" required placeholder="your Age"
										style="margin-bottom: 10px" type="text" />
								</div>
							</div>
							<div class="form-group">
								<label for="id_company"
									class="control-label col-md-4  requiredField"> Current
									Health<span class="asteriskField">*</span>
								</label>
								<div class="controls col-md-4 ">
									<label class="checkbox-inline"> <input type="checkbox" name="health"
										value="Hypertension">Hypertension
										</label>
								</div>
								<div class="controls col-md-4 ">
									<label class="checkbox-inline"> <input type="checkbox" name="health"
										value="Blood pressure">Blood pressure
										</label>
								</div>
							</div>
								<div class="form-group">
								
								<div class="controls col-md-4 ">
									 <label class="checkbox-inline"> <input type="checkbox" name="health"
										value="Blood sugar">Blood sugar
										</label>
								</div>
								<div class="controls col-md-4 ">
									 <label class="checkbox-inline"> <input type="checkbox" name="health"
										value="Overweight">Overweight
									</label>
								</div>
							</div>
							<div class="clearfix">&nbsp;&nbsp;</div>
							<div class="form-group">
								<label for="id_company"
									class="control-label col-md-4  requiredField"> Habits
									<span class="asteriskField">*</span>
								</label>
								<div class="controls col-md-4 ">
									<label class="checkbox-inline"> <input type="checkbox" name="habits"
										value="Smoking">Smoking 
										</label>
								</div>
								<div class="controls col-md-4 ">
									 <label class="checkbox-inline"> <input type="checkbox" name="habits"
										value="Alcohol">Alcohol
										</label>
								</div>
								</div>
								<div class="form-group">
								
								<div class="controls col-md-4 ">
									 <label class="checkbox-inline"> <input type="checkbox" name="habits"
										value="Daily exercise">Daily exercise
										</label>
								</div>
								<div class="controls col-md-4 ">
									 <label class="checkbox-inline"> <input type="checkbox" name="habits"
										value="Drugs">Drugs
									</label>
								</div>
							</div>
							<div class="clearfix">&nbsp;&nbsp;</div>
							<div class="form-group">
								<div class="aab controls col-md-4 "></div>
								<div class="controls col-md-8 ">
									<input type="submit" name="submit" value="Submit"
										class="btn btn-primary btn btn-info" id="submit-id-signup" />
									
								</div>
							</div>

						</form>

					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>

